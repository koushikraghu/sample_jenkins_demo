FROM python:3.6
RUN apt-get update
RUN mkdir /app
WORKDIR /app
COPY source /app/source
RUN pip3 install -U -r /app/source/requirements.txt
EXPOSE 4545
CMD ["python","source/hello.py"]